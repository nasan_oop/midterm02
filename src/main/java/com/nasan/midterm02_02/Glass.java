/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.midterm02_02;

/**
 *
 * @author nasan
 */
public class Glass extends WaterWare {

    public Glass(int maxVol, String name) {
        super(maxVol, name);
        System.out.println("This glass (" + this.name + ") maximum contain is " + this.maxVol + " ml.");//เมื่อ object ถูกสร้างขึ้น จะแสดงข้อความบอกว่าเก็บได้สูงสุดเท่าไหร่ และตอนนี้มีน้ำอยู่เท่าไหร่
    }

    @Override
    public int getVol() {
        return vol;
    }

    @Override
    public int getMaxVol() {
        return maxVol;
    }

    @Override
    public void setVol(int vol) {
        if (vol > maxVol) { //ใช้ตรวจสอบ หากใส่ปริมาณน้ำมากกว่าที่แก้วรับได้ น้ำที่เกินมาจะล้น ปริมาณน้ำจะเท่ากับปริมาณสูงสุดที่น้ำจะเก็บได้
            this.vol = this.maxVol;
            System.out.println("this glass can contain only " + this.maxVol + " ml. ");
        } else {
            this.vol = vol;
        }
        System.out.println("This glass (" + this.name + ") maximum contain is " + this.maxVol + " ml, And now it's have " + this.vol + " ml.");
    }

    @Override
    public int reduce(int volOfdrink) {//method สำหรับลดปริมาณน้ำในแก้ว โดยรับปริมาณน้ำที่จะลดลงไปจาก method drink ในคลาส Human
        return this.vol = this.vol - volOfdrink;
    }

    @Override
    public String toString() {//บอกปริมาณน้ำที่เหลือในแก้ว
        return "This glass (" + this.name + ") have " + vol + " ml. left";
    }
}
