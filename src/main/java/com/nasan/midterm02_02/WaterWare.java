/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.midterm02_02;

/**
 *
 * @author nasan
 */
public class WaterWare {

    protected int maxVol;//ตัวแปรกำหนดค่าสูงสุดที่ใส่น้ำได้(กำหนดปริมาตรของภาชนะ)
    protected int vol = 0;//ตัวแปรกำหนดปริมาณน้ำที่จะใส่ไปในภาชนะ โดยเริ่มต้นทุกภาชนะจะมีน้ำเป็น 0
    protected String name;//ตัวแปรเก็บชื่อภาชนะ

    public WaterWare(int maxVol, String name) {//constructor กำหนดให้ใส่ปริมาตรและชื่อ
        this.maxVol = maxVol;
        this.name = name;
    }

    public int getVol() {
        return vol;
    }

    public int getMaxVol() {
        return maxVol;
    }

    public void setVol(int vol) {
        if (vol > maxVol) { //ใช้ตรวจสอบ หากใส่ปริมาณน้ำมากกว่าที่ภาชนะรับได้ น้ำที่เกินมาจะล้น ปริมาณน้ำจะเท่ากับปริมาณสูงสุดที่น้ำจะเก็บได้
            this.vol = this.maxVol;
            System.out.println("Can contain only " + this.maxVol + " ml. ");
        } else {
            this.vol = vol;
        }
        System.out.println(this.name + " maximum contain is " + this.maxVol + " ml, And now it's have " + this.vol + " ml.");
    }

    public int reduce(int volOfdrink) {//method สำหรับลดปริมาณน้ำในภาชนะ โดยรับปริมาณน้ำที่จะลดลงไปจาก method drink ในคลาส Human
        return this.vol = this.vol - volOfdrink;
    }

    @Override
    public String toString() {//บอกปริมาณน้ำที่เหลือในภาชนะ
        return this.name + " have " + vol + " ml. left";
    }
}
