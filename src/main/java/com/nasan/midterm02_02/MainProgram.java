/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.midterm02_02;

/**
 *
 * @author nasan
 */
public class MainProgram {

    public static void main(String[] args) {
        int maxdrink = Integer.MIN_VALUE; // ตัวแปรสำหรับเก็บปริมาณน้ำของคนที่ดื่มไปมากที่สุด

        Glass glass01 = new Glass(500, "glass01"); //สร้าง object
        glass01.setVol(450);//กำหนดปริมาณน้ำในแก้ว
        Glass glass02 = new Glass(500, "glass02");//สร้าง object
        glass02.setVol(470);//กำหนดปริมาณน้ำในแก้ว
        Glass glass03 = new Glass(500, "glass03");// ทดสอบกรณีน้ำที่ใส่เกินปริมาตรของแก้ว //สร้าง object
        glass03.setVol(700);//กำหนดปริมาณน้ำในแก้ว
        Glass glass04 = new Glass(580, "glass04");//สร้าง object
        glass04.setVol(130);//กำหนดปริมาณน้ำในแก้ว
        Glass glass05 = new Glass(1500, "glass05");//สร้าง object
        glass05.setVol(1300);//กำหนดปริมาณน้ำในแก้ว
        Glass glass06 = new Glass(950, "glass06");//สร้าง object
        glass06.setVol(800);//กำหนดปริมาณน้ำในแก้ว
        Glass glass07 = new Glass(200, "glass07");//สร้าง object
        glass07.setVol(100);//กำหนดปริมาณน้ำในแก้ว
        Glass glass08 = new Glass(580, "glass08");//สร้าง object
        glass08.setVol(500);//กำหนดปริมาณน้ำในแก้ว
        Glass glass09 = new Glass(720, "glass09");//สร้าง object
        glass09.setVol(600);//กำหนดปริมาณน้ำในแก้ว
        Glass glass10 = new Glass(920, "glass10");//สร้าง object
        glass10.setVol(860);//กำหนดปริมาณน้ำในแก้ว

        Human human01 = new Human("A");//สร้าง object
        Human human02 = new Human("B");//สร้าง object
        Human human03 = new Human("C");//สร้าง object
        Human human04 = new Human("D");//สร้าง object
        Human human05 = new Human("E");//สร้าง object
        Human humans[] = {human01, human02, human03, human04, human05};//นำคนเก็บใน array

        human01.drink(420, glass01);
        human02.drink(570, glass02);     // ทดสอบกรณีสั่งให้ดื่มน้ำในปริมาณมากกว่าน้ำที่มีอยู่ในแก้ว
        human02.drink(570, glass02);    // ทดสอบกรณีที่สั่งให้ดื่มน้ำในแก้วที่ไม่มีน้ำ
        human03.drink(300, glass03);
        human04.drink(280, glass04);
        human05.drink(160, glass05);

        human01.drink(glass07); //สั่งให้ดื่มน้ำจนหมดแก้ว
        human02.drink(glass06);//สั่งให้ดื่มน้ำจนหมดแก้ว
        human03.drink(glass08);//สั่งให้ดื่มน้ำจนหมดแก้ว
        human04.drink(glass09);//สั่งให้ดื่มน้ำจนหมดแก้ว
        human05.drink(glass10);//สั่งให้ดื่มน้ำจนหมดแก้ว

        findWinner(humans, maxdrink); // เรียกใช้ findWinner

    }

    public static void findWinner(Human[] humans, int maxdrink) {//method สำหรับหาคนที่ดื่มน้ำไปมากที่สุด
        int maxCount = 0; //ตัวแปรเก็บจำนวนคนที่มีปริมาณน้ำเท่ากับปริมาณสูงสุด
        String namemax = "";//เก็บชื่อผู้ชนะ
        for (int i = 0; i < 5; i++) {// loop สำหรับหาค่ามากที่สุด
            if (humans[i].getTotalOfdrink() > maxdrink) {
                maxdrink = humans[i].getTotalOfdrink();//เก็บจำนวนน้ำที่ดื่มไปทั้งหมดของคนคนนั้น
                namemax = humans[i].getName();//เก็บชื่อของคนคนนั้น
            }
        }
        for (int i = 0; i < 5; i++) {//loop สำหรับนับว่ามีคนได้คะแนนสูงสุดเท่ากันกี่คน
            if (humans[i].getTotalOfdrink() == maxdrink) {//หากมีให้ maxCount +1
                maxCount++;
            }
        }
        if (maxCount > 1) {//หากมีมากกว่า 1 คน แสดงข้อความไม่มีผู้ชนะ หากมีแค่คนเดียวแสดงชื่อผู้ชนะ
            System.out.println("No winner");
        } else {
            System.out.println("The winner is " + namemax);
        }

    }
}
